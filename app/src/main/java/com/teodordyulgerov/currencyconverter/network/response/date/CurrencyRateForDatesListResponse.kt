package com.teodordyulgerov.currencyconverter.network.response.date

data class CurrencyRateForDatesListResponse(

    val pairs: List<CurrencyRateForDatesResponse>
)