package com.teodordyulgerov.currencyconverter.network.response.currency

data class CurrencyListResponse(

    val currencies: List<CurrencyResponse>
)