package com.teodordyulgerov.currencyconverter.network.response.realtime

data class CurrencyRateListResponse(

    val rates: List<CurrencyRateResponse>
)