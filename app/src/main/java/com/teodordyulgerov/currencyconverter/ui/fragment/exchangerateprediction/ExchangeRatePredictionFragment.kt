package com.teodordyulgerov.currencyconverter.ui.fragment.exchangerateprediction

import com.teodordyulgerov.currencyconverter.R
import com.teodordyulgerov.currencyconverter.databinding.FragmentExchangeRatePredictionBinding
import com.teodordyulgerov.currencyconverter.ui.fragment.BaseFragment

class ExchangeRatePredictionFragment : BaseFragment<ExchangeRatePredictionVM, FragmentExchangeRatePredictionBinding>() {

    override val viewModelClass = ExchangeRatePredictionVM::class

    override fun getLayoutResId(): Int = R.layout.fragment_exchange_rate_prediction

    override fun getTitle(): Int = R.string.title_exchange_rate_prediction_fragment
}