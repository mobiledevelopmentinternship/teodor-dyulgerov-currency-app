package com.teodordyulgerov.currencyconverter.ui.fragment.home

import com.teodordyulgerov.currencyconverter.R
import com.teodordyulgerov.currencyconverter.databinding.FragmentHomeBinding
import com.teodordyulgerov.currencyconverter.ui.fragment.BaseFragment

class HomeFragment : BaseFragment<HomeVM, FragmentHomeBinding>() {

    override val viewModelClass = HomeVM::class

    override fun getLayoutResId(): Int = R.layout.fragment_home

    override fun shouldHideActionBar(): Boolean = true
}