package com.teodordyulgerov.currencyconverter.ui.fragment.exchangerateprediction

import com.teodordyulgerov.currencyconverter.ui.fragment.BaseVM
import javax.inject.Inject

class ExchangeRatePredictionVM
@Inject constructor() : BaseVM()