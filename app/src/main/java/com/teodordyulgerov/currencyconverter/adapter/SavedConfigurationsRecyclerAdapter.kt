package com.teodordyulgerov.currencyconverter.adapter

import android.databinding.ObservableArrayList
import com.teodordyulgerov.currencyconverter.ui.fragment.BaseVM

class SavedConfigurationsRecyclerAdapter<T>(
    viewModel: BaseVM,
    items: ObservableArrayList<T>,
    private val layoutId: Int
) : BaseRecyclerAdapter<T>(viewModel, items) {

    override fun getLayoutId(): Int = layoutId
}