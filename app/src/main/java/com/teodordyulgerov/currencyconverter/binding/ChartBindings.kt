package com.teodordyulgerov.currencyconverter.binding

import android.databinding.BindingAdapter
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.data.LineData
import com.teodordyulgerov.currencyconverter.App
import com.teodordyulgerov.currencyconverter.persistence.model.ConversionRatePersist
import com.teodordyulgerov.currencyconverter.utils.configureLineChart
import com.teodordyulgerov.currencyconverter.utils.createLineDataSet
import com.teodordyulgerov.currencyconverter.utils.getLineDataSetValues

@BindingAdapter("chartData")
fun setChartData(chart: LineChart, chartData: List<ConversionRatePersist>) {
    if (chartData.isNotEmpty()) {
        val context = App.instance.applicationContext
        val dataSetValues = getLineDataSetValues(chartData)
        configureLineChart(context, chart, dataSetValues.second)
        chart.data = LineData(listOf(createLineDataSet(context, dataSetValues.first)))
        chart.animateX(250)
    }
}