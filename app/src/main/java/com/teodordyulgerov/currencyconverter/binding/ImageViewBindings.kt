package com.teodordyulgerov.currencyconverter.binding

import android.databinding.BindingAdapter
import android.widget.ImageView
import com.teodordyulgerov.currencyconverter.utils.PicassoImageLoader

@BindingAdapter("imgViewSrc")
fun setImgViewSrc(imgView: ImageView, currencyId: String?) = PicassoImageLoader.setImage(imgView, currencyId)