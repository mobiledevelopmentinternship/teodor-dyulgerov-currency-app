package com.teodordyulgerov.currencyconverter.binding

import android.databinding.BindingAdapter
import android.databinding.ObservableArrayList
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.LinearLayout
import com.teodordyulgerov.currencyconverter.adapter.BaseRecyclerAdapter
import com.teodordyulgerov.currencyconverter.adapter.SavedConfigurationsRecyclerAdapter
import com.teodordyulgerov.currencyconverter.adapter.SearchCurrenciesRecyclerAdapter
import com.teodordyulgerov.currencyconverter.ui.fragment.BaseVM
import com.teodordyulgerov.currencyconverter.ui.fragment.savedconfigurations.SavedConfigurationsVM
import com.teodordyulgerov.currencyconverter.ui.fragment.searchcurrencies.SearchCurrenciesVM

@BindingAdapter("viewModel", "items", "itemLayoutId", requireAll = true)
fun <T> bindRecyclerView(recView: RecyclerView, viewModel: BaseVM, items: ObservableArrayList<T>, itemLayoutId: Int) {
    if (recView.layoutManager == null) {
        recView.layoutManager = LinearLayoutManager(recView.context, LinearLayout.VERTICAL, false)
    }

    if (recView.adapter == null) {
        if (viewModel is SearchCurrenciesVM) {
            recView.adapter = SearchCurrenciesRecyclerAdapter(viewModel, items, itemLayoutId)

            return
        }
        if (viewModel is SavedConfigurationsVM) {
            recView.adapter = SavedConfigurationsRecyclerAdapter(viewModel, items, itemLayoutId)

            return
        }
    } else {
        (recView.adapter as BaseRecyclerAdapter<T>).setItems(items)
    }
}