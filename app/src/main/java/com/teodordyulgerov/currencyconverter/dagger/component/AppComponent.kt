package com.teodordyulgerov.currencyconverter.dagger.component

import com.teodordyulgerov.currencyconverter.dagger.module.AppModule
import com.teodordyulgerov.currencyconverter.dagger.module.NetworkModule
import com.teodordyulgerov.currencyconverter.dagger.module.RoomModule
import com.teodordyulgerov.currencyconverter.dagger.module.ViewModelModule
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        AppModule::class,
        RoomModule::class,
        NetworkModule::class,
        ViewModelModule::class
    ]
)
interface AppComponent : AndroidInjector<DaggerApplication>